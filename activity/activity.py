# 1.

year = int(input("Please input a year \n"))

if year % 4 == 0:
	print(f"{year} is a leap year")
else:
	print(f"{year} is not a leap year")


# 2.


rows = int(input("Row: \n "))
cols = int(input("Column: \n "))

for i in range(rows):
    for j in range(cols):
        print("*", end=" ")
    print()  
