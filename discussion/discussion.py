# Input
#  input() is similar to prompt() in JS that seeks to gather data from user input
# \n stands for line break
# username = input("Please enter your name: \n")
# print(f"Hi {username}! Welcome to Python Short Course!")

# num1 = input("Enter 1st number: \n")
# num2 = input("Enter 2nd number: \n")
# print(f"The sum of num1 and num2 is {num1 + num2}")
# print(type(num1))
# print(type(num2))
# in JS = typeof
# in Python = type()
# num1 = int(input("Enter 1st number: \n"))
# num2 = int(input("Enter 2nd number: \n"))
# print(f"The sum of num1 and num2 is {num1 + num2}")


# If-Else Statement
# Control Structures
	# 1. Selection
		# allow the program to choose among choices and run specific codes depending on the choice taken
	# 2. Repetition
		# allow the program to repeat certain blocks of code given a starting condition and terminating condition


# If-Else Statement
# test_num = 75

# if test_num >=60:
# 	print("Test passed")
# else:
# 	print("Test failed")


# # If Else chains
# test_num2 = int(input("Please enter the 2nd test number \n"))

# if test_num2 > 0:
# 	print("The number is positive")
# elif test_num2 == 0:
# 	print("The number is zero")
# else:
# 	print("The number is negative")

# The elif is the shorthand for "else if" in other programming language

# Mini activity

# num = int(input("Enter number \n"))

# if num % 3 == 0 and num % 5 == 0:
# 	print("The number is divisible by both 3 and 5")
# elif num % 5 == 0:
# 	print("The number is divisible by 5")
# elif num % 3 == 0:
# 	print("The number is divisible by 3")
# else:
# 	print("The number is not divisible by 3 nor 5")

# Loops
# Python has loops that can repeat blocks of codes

# While Loops are used to execute a set of statements as long as the condition is true

# i = 1
# while i <= 5:
# 	print(f"The current count {i}")
# 	i += 1


# For loops are used for iterating over a sequence
fruits = ["apple", "banana", "cherry"]
for indiv_fruit in fruits:
	print(indiv_fruit)


# range() method - returns the sequence of the given number
# Syntax:
	# range(stop)
	# range(start, stop)
	# range(start, stop, step)

# e 
# The range() functions defaults to 0 as a starting value. As such, this prints the values from 0 to 5

# for x in range(6, 10):
# 	print(f"The current value is {x}")
# # This prints the value from 6 to 9. Always remember that the range always prints n-1


# for x in range(6, 20, 2):
# 	print(f"The current value is {x}")
# A third argument can be added to specify the increment of the loop.


# Mini activity

# Create a loop to count and display the number of even numbers between 1 and 20
# Print "The number of even nubmer between 1 to 20 is: <a total number of even numbers>"

# count = 0 #counter

# for x in range (1, 21):
# 	if x % 2 == 0:
# 		count += 1

# print(f"The number of even numbers between 1 to 20 is: {count}")


# Mini Exercise:
# Write a Python program that takes an integer input from the user and display the multiplication table for that number. From 1 to 10

# num = int(input("Enter number \n"))

# for x in range (1, num + 1):
# 	result = num * x
# 	print(f"{num} x {x} = {result}")


# Break Statement - is used to stop the loop

# j = 1
# while j < 6:
# 	print(j)
# 	if j == 3:
# 		break
# 	j += 1


# Continue Statement - returns the control to the beginning of the while loop and continue with the next iteration

k = 1
while k < 6:
	k += 1
	if k == 3:
		continue
	print(k)